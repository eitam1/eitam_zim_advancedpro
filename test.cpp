#include "shiftText.h"
#include "substitutionText.h"
#include "shift3Text.h"
#include "DecryptException.h"
#include <iostream>

#define DICTIONARY_PATH "dictionary.csv"

using namespace std;

int main()
{
	//testing the shift-3 mode
	shift3Text msg("hello my name is Alice");
	cout << msg.getText() << endl;
	try
	{
		msg.decrypt();
		cout << msg.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}
	try
	{
		msg.decrypt();
		cout << msg.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}

	//testing the substitution mode
	fstream dic;
	dic.open(DICTIONARY_PATH);
	substitutionText msg2("rari",dic);
	cout << msg2.getText() << endl;
	try
	{
		msg2.decrypt(dic);
		cout << msg2.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}
	try //trying to decrypt the second time
	{
		msg2.decrypt(dic);
		cout << msg2.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}
	dic.close();
	getchar();
	return 0;
}