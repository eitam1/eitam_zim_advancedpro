#include "shiftText.h"
#include "substitutionText.h"
#include "shift3Text.h"
#include "DecryptException.h"
#include <iostream>

#define SHIFTING_KEY 10
#define DICTIONARY_PATH "dictionary.csv"

using namespace std;

void Alice();
PlainText Bob(substitutionText msg);
PlainText Bob(shiftText msg);
PlainText Bob(shift3Text msg);

/*
This is the main function of the program.
It starts the conversation between Alice and Bob
*/
int main()
{
	Alice();
	
	cout << endl;
	system("PAUSE");
	return 0;
}

void Alice()
{
	string str;
	cout << "Alice:" << endl;
	cout << "Enter a message to send to Bob:" << endl;
	getline(cin, str);
	cout << endl;

	PlainText receive("");

	cout << "Sending in Substitution mode..." << endl;
	fstream dic;
	dic.open(DICTIONARY_PATH);
	receive = Bob(substitutionText(str, dic));
	cout << endl << "Alice:" << endl << "Received back: " << receive.getText() << endl;

	cout << "Sending in Shift mode..." << endl;
	receive = Bob(shiftText(str, SHIFTING_KEY));
	cout << endl << "Alice:" << endl << "Received back: " << receive.getText() << endl;

	cout << "Sending in Shift-3 mode..." << endl;
	receive = Bob(shift3Text(str));
	cout << endl << "Alice:" << endl << "Received back: " << receive.getText() << endl;
}
/*
This is Alice's part in the conversation.
It gets a legal string from the user ans sends it to Bob in
three different kinds of encryptions.
It also prints every response from Bob.
*/
PlainText Bob(substitutionText msg)
{
	fstream dic;
	dic.open(DICTIONARY_PATH);
	cout << endl << "Bob:" << endl;
	cout << "The received message: " << msg.getText() << endl;
	try
	{
		msg.decrypt(dic);
		cout << "The decrypted message is: " << msg.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}
	dic.close();
	return PlainText("Thank you Alice!");
}

/*
This function handles Bob's part of the conversation
when he receives a message encrypted in Shift mode
*/
PlainText Bob(shiftText msg)
{
	cout << endl << "Bob:" << endl;
	cout << "The received message: " << msg.getText() << endl;
	try
	{
		msg.decrypt(SHIFTING_KEY);
		cout << "The decrypted message is: " << msg.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}
	return PlainText("Thank you again Alice!");
}

/*
This function handles Bob's part of the conversation
when he receives a message encrypted in Shift-3 mode
*/
PlainText Bob(shift3Text msg)
{
	cout << endl << "Bob:" << endl;
	cout << "The received message: " << msg.getText() << endl;
	try
	{
		msg.decrypt();
		cout << "The decrypted message is: " << msg.getText() << endl;
	}
	catch (DecryptException de)
	{
		cout << de.what() << endl;
	}
	return PlainText("Many Thanks Alice!");
}