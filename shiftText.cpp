#include "shiftText.h"
#include "DecryptException.h"

shiftText::shiftText(string text, int key) :PlainText(encrypt(text, key))
{
	_isEnc = true;
}

string shiftText::encrypt(string text, int key)
{
	for (int i = 0; i < text.length(); i++)
	{
		
		if (text[i] < 97 || text[i] > 122)
		{
			text[i] = text[i];
			continue;
		}
		else if (text[i] + key > 122)
		{
			text[i] = text[i] -(122 - key) +96;
			continue;
		}
		text[i] = text[i] + key;
	}
	return text;

}

void shiftText::decrypt(int key)
{
	if (!isEnc())
	{
		DecryptException a;
		throw (a);
	}
	for (int i = 0; i < _text.length(); i++)
	{
		if (_text[i] < 97 || _text[i] > 122)
		{
			_text[i] = _text[i];
			continue;
		}
		else if (_text[i] - key < 97)// checks if its lower then a
		{
			_text[i] = _text[i] + (26 - key);
			continue;
		}
		_text[i] = _text[i] - key;
	}
	_isEnc = false;
}
shiftText::~shiftText()
{}