#ifndef substitutionText_h
#define substitutionText_h
#include "PlainText.h"
#include <fstream>
class substitutionText : public PlainText
{
public:
	substitutionText(string text, fstream& dictionary);
	~substitutionText();
	void decrypt(fstream& dictionary);
private:
	string encrypt(string text, fstream& dictionary);
};
#endif