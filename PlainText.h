#ifndef PlainText_h
#define PlainText_h
#include <iostream>
#include <string>
using namespace std;
class PlainText
{
protected:
	string _text;
	bool _isEnc;
public:
	PlainText(string text);
	~PlainText();
	string getText();
	bool isEnc();
};
#endif