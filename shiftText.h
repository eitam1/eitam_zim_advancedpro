#ifndef shiftText_h
#define shiftText_h
#include "PlainText.h"
using namespace std;
class shiftText: public PlainText
{
public:
	shiftText(string text, int key);
	~shiftText();
	void decrypt(int key);
private:
	string encrypt(string text, int key);
};
#endif